// Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos", 
	{
		method: "GET",
		headers: {
			"Content-Type": "application/json"
		}
	}
)
.then((response) => response.json())
.then((response) => console.log(response));
// Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => {
	let list = json.map((todo => {
		return todo.title;
	}))
	console.log(list);
});



//  Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos/1", 
	{
		method: "GET",
		headers: {
			"Content-Type": "application/json"
		}
	}
)
.then((response) => response.json())

// Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));


// Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos", 
	{
		method: "POST",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			userId: 1,
			title: "Lorem Ipsum",
			completed: false
		})
	}
)
.then(response => response.json())
.then(json => console.log(json));


// Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1", 
	{
		method: "PUT",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			userId: 1,
			Id: 1,
			title: "Lorem Ipsumae",
			completed: true
		})
	}
)
.then(response => response.json())
.then(response => console.log(response));

// Update a to do list item by changing the data structure to contain the following properties:
// - Title
// - Description
// - Status
// - Date Completed
// - User ID

fetch("https://jsonplaceholder.typicode.com/todos/1", 
	{
		method: "PUT",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			title: "Lorem Ipsum",
			description: "Just a random of anything",
			status: "Pending",
			dateCompleted: "Pending",
			userId: 1
		})
	}
)
.then(response => response.json())
.then(json => console.log(json));

// Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1", 
	{
		method: "PATCH",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			status: "Complete",
			dateCompleted: "January 12, 2023",
		})
	}
)
.then(response => response.json())
.then(response => console.log(response));

// Update a to do list item by changing the status to complete and add a date when the status was changed.

fetch("https://jsonplaceholder.typicode.com/todos/1", 
	{
		method: "PATCH",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			status: "Incomplete",
			dateCompleted: "January 11, 2023"
		})
	}
)
.then(response => response.json())
.then(response => console.log(response));

//  Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1", 
	{
		method: "DELETE",
	}
)
.then(response => response.json())
.then(response => console.log(response));